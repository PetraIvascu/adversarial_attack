"""
resnet50_v2.py config file containig various items useful for
Resnet50V2 class
"""
from __future__ import absolute_import

params = {
    "input_node_name": "input_3:0",
    "output_node_name": "probs/Softmax:0",
    "size": 224,
    "no_classes": 1000,
    "epsilon": 0.007
}