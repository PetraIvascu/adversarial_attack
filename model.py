"""
model.py module contains class which uses a mobilenet v2 pb in order to
load graph, do inference, display results all of this for a classification task
"""

import glob
import json
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
from path_config import MODEL, DATASET


class Resnet50V2:
  """class responsible with loading, inferencing, diplaying results
  """

  def __init__(self, input_node_name, output_node_name):
    self.sess = tf.Session()
    self.frozen_graph = glob.glob("{}/*.pb".format(MODEL))
    self.input_node, self.output_node = self.load(input_node_name,
                                                  output_node_name)

  def __call__(self, target_label, no_classes=1000, eps=0.007):
    """
    Arguments:
        target_label {int} -- target label for adv. attack

    Keyword Arguments:
        eps {float} -- small error added to each pixel (default: {0.007})
    """
    target_class = tf.one_hot(target_label, no_classes)
    one_hot_target_class = tf.reshape(target_class, [1, no_classes])
    self.adv_image_node, self.noise = self.step_targeted_attack(
        self.input_node, eps, one_hot_target_class, self.output_node)

  def load(self, input_n_name, output_n_name):
    """loads the pb found in frozen_graph directory

    Arguments:
        input_n_name {string} -- name of the input node
        output_n_name {string} -- name of the output node

    Returns:
        tf.float_32 -- computation graph node for input
        tf.float_32 -- computation graph node for output
    """

    with tf.gfile.GFile(self.frozen_graph[0], 'rb') as f:
      graph_def = tf.GraphDef()
      graph_def.ParseFromString(f.read())
    self.sess.graph.as_default()
    tf.import_graph_def(graph_def, name='')

    output_node = self.sess.graph.get_tensor_by_name(output_n_name)
    input_node = self.sess.graph.get_tensor_by_name(input_n_name)
    self.graph = self.sess.graph
    return input_node, output_node

  def predict(self, image):
    """predicts using loaded graph

    Arguments:
        image {float32} -- image as a np. ndarray

    Returns:
        float32 -- array with values of confidence for each class
    """
    result = self.sess.run(self.output_node, feed_dict={self.input_node: image})

    return result[0]

  def display_image(self, image, description):
    """plots input for net and the output received using human readble labels
    and the confidance value

    Arguments:
        image {float32} -- image to be plotted
        description {string} -- info to add to plotted image
    """
    if not isinstance(description, str):
      raise TypeError("description must be of type str")

    plt.figure(figsize=(15, 10))
    plt.axis("off")
    plt.title(description, fontsize=22, fontweight="bold")
    plt.imshow(image)
    plt.show()

  def label_mapper(self, idx, filename=DATASET):
    """maps given index to a human readable label

    Arguments:
        idx {int} -- index representing the position where highest confidance
        can be found

    Returns:
        string -- human readable label (e.g. doormat, cat ...)
    """

    with open(filename, 'r') as f:
      label_map = json.loads(f.read())
    human_readable_label = label_map[str(idx)][1]
    return human_readable_label

  def step_targeted_attack(self, x, eps, one_hot_target_class, logits):
    """adversarial attack

    Arguments:
        x {tf graph node float32} -- input node
        eps {float32} -- small error added to each pixel
        one_hot_target_class {float32} -- dummy vector encoding,
        mostly filled with zeros except to a position which has 1.0 for the
        class it represents
        logits {tf graph node float32} -- output node used for predictions

    Returns:
        [tf graph node float32] -- node affected by the noise created
        [tf graph node float32] -- node containing the noise
    """
    cross_entropy = tf.losses.softmax_cross_entropy(one_hot_target_class,
                                                    logits)
    noise = eps * tf.sign(tf.gradients(cross_entropy, x)[0])
    x_adv = x - noise
    x_adv = tf.clip_by_value(x_adv, -1.0, 1.0)

    return tf.stop_gradient(x_adv), noise

  def apply_adversarial_attack(self, image, no_iterations=10):
    """applies iteratively an adv. attack

    Arguments:
        image {float32} -- image as a np. ndarray
    Keyword Arguments:
        no_iterations {int} -- number of iterations to perform (default: {10})

    Returns:
        [float32] -- image on top applied adv. attack
        [float32] -- array of predictions for above image
        [float32] -- the carefully computed noise added to original image
    """

    adv_image = image
    adv_noise = np.zeros(image.shape)

    for _ in range(no_iterations):
      adv_image, a = self.sess.run((self.adv_image_node, self.noise),
                                   {self.input_node: adv_image})
      adv_noise = adv_noise + a

    prediction = self.predict(adv_image)

    return adv_image, prediction, adv_noise
