"""
test_model.py module which tests the class functionalities
"""
import os
import sys
import unittest
import numpy as np
import tensorflow as tf

where, _ = os.path.split(os.path.realpath(__file__))
sys.path.append(where)

from model import Resnet50V2


class Resnet50V2Test(unittest.TestCase):
  """test class which challenges functionalieties of class Resnet50_V2

  Arguments:
      unittest -- inherits TestCase
  """

  def __init__(self, *args, **kwargs):
    super(Resnet50V2Test, self).__init__(*args, **kwargs)
    self.resnet50_v2 = Resnet50V2(
        input_node_name="input_3:0", output_node_name="probs/Softmax:0")
    self.mock_image = np.random.uniform(
        low=-1.0, high=1.0, size=(1, 224, 224, 3))
    self.mock_image_wrong_size = np.random.uniform(
        low=-1.0, high=1.0, size=(1, 200, 200, 3))
    self.mock_image_wrong_shape = np.random.uniform(
        low=-1.0, high=1.0, size=(224, 224, 3))
    self.mock_image_single_channel = np.random.uniform(
        low=-1.0, high=1.0, size=(1, 224, 224, 1))
    self.mock_prediction = np.random.dirichlet(np.ones(1000), size=1)[0]

  def test_setup_wrong_nodes(self):
    input_node_name = "image:0"
    output_node_name = "output:0"
    self.assertRaises(KeyError, Resnet50V2, input_node_name, output_node_name)

  def test_input_shape_nodes(self):
    self.assertEqual(self.resnet50_v2.input_node.get_shape().as_list(),
                     [None, 224, 224, 3])

  def test_input_type_node(self):
    self.assertEqual(self.resnet50_v2.input_node.dtype, tf.float32)

  def test_output_shape_nodes(self):
    self.assertEqual(self.resnet50_v2.output_node.get_shape().as_list(),
                     [None, 1000])

  def test_output_type_node(self):
    self.assertEqual(self.resnet50_v2.output_node.dtype, tf.float32)

  def test_shape_prediction(self):
    self.prediction = self.resnet50_v2.predict(self.mock_image)
    self.assertEqual(self.prediction.shape, (1000,))

  def test_prediction_image_wrong_size(self):
    self.assertRaises(ValueError, self.resnet50_v2.predict,
                      self.mock_image_wrong_size)

  def test_prediction_image_no_batch_dim(self):
    self.assertRaises(ValueError, self.resnet50_v2.predict,
                      self.mock_image_wrong_shape)

  def test_prediction_image_single_channel(self):
    self.assertRaises(ValueError, self.resnet50_v2.predict,
                      self.mock_image_single_channel)

  def test_label_map_file_not_found(self):
    filename = "./images/abc.json"
    self.assertRaises(FileNotFoundError, self.resnet50_v2.label_mapper,
                      np.argmax(self.mock_prediction, axis=0), filename)

  def test_type_label(self):
    label = self.resnet50_v2.label_mapper(
        np.argmax(self.mock_prediction[0], axis=0))
    print(type(label))
    print(isinstance(label, str))
    self.assertTrue(isinstance(label, str))

  def test_display_image_descrition_not_string(self):
    not_a_string = 1234
    self.assertRaises(TypeError, self.resnet50_v2.display_image,
                      self.mock_image, not_a_string)


if __name__ == '__main__':
  unittest.main()
