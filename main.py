"""main.py module calling Resnet50V2 class and other methods to creat adv. attacks
"""
import numpy as np
from PIL import Image
from model import Resnet50V2
from configs import configs


def preprocess_image(image, size):
  """preprocessing the image accordingly to resnet50v2 normalization (-1,1)

  Arguments:
      image {float32} -- image
      size {int} -- desired size for the given image

  Returns:
      float32 -- processed image
  """
  resized_image = image.resize((size, size), Image.ANTIALIAS)
  processed_image = np.asarray(resized_image, dtype=np.float32) / 127.5 - 1
  processed_image = np.expand_dims(processed_image, axis=0)

  return processed_image


def main():
  config = configs["resnet50_v2"]
  # read and preprocess image
  image_file = "./images/meerkat.jpg"
  image = Image.open(image_file)
  processed_image = preprocess_image(image, config["size"])

  # creating object Resnet50V2 -> inference
  model = Resnet50V2(config["input_node_name"], config["output_node_name"])
  real_prediction = model.predict(processed_image)
  least_likely_class = np.argmin(real_prediction, axis=0)

  # adding to the current graph nodes for adverserial attack
  model(
      least_likely_class,
      no_classes=config["no_classes"],
      eps=config["epsilon"])

  # apply attack on processed image above
  adversarial_image, adversarial_prediction, noise = model.apply_adversarial_attack(
      processed_image, no_iterations=10)

  # descrition for plots
  description_real_prediction = "{} : {:.2f}% Confidence".format(
      model.label_mapper(np.argmax(real_prediction, axis=0)),
      np.max(real_prediction, axis=0) * 100)

  description_adverserial_prediction = "{} : {:.2f}% Confidence".format(
      model.label_mapper(np.argmax(adversarial_prediction, axis=0)),
      np.max(real_prediction, axis=0) * 100)

  description_noise = "Noise generated with epsilon: {:.3f}".format(
      config["epsilon"])

  # display image, confidance and class
  model.display_image(processed_image[0] / 2 + 0.5, description_real_prediction)
  model.display_image(adversarial_image[0] / 2 + 0.5,
                      description_adverserial_prediction)
  model.display_image(noise[0] / 2 + 0.5, description_noise)


if __name__ == "__main__":
  main()