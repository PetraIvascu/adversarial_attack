"""
path_config.py module contains a series of very used paths by class Resnet50V2
"""
import os

ROOT = os.path.dirname(os.path.realpath(__file__))
MODEL = os.path.join(ROOT, 'frozen_graph')
DATASET = os.path.join(ROOT, 'dataset', 'imagenet_class_index.json')
